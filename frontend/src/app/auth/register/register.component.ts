import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      firstname: new FormControl(null, {
          validators: [Validators.required]
      }),
      lastname: new FormControl(null, {
        validators: [Validators.required]
      }),
      image: new FormControl(null, {
        validators: []
      }),
      username: new FormControl(null, {
        validators: [Validators.required]
      }),
      password: new FormControl(null, {
        validators: [Validators.required]
      }),
      passwordConfirm: new FormControl(null, {
        validators: [Validators.required]
      }),
      birthdate: new FormControl(null, {
        validators: [Validators.required]
      }),
      country: new FormControl(null, {
        validators: [Validators.required]
      }),
      city: new FormControl(null, {
        validators: [Validators.required]
      }),
      email: new FormControl(null, {
        validators: [Validators.required, Validators.email]
      })
    });

  }

  onImagePicked(files: File[]) {
    // console.log(files[0]);
    this.form.patchValue({image: files[0]});
    this.form.get('image').updateValueAndValidity();
  }

  resolved(captchaResponse: string) {
    this.authService.checkCaptcha({ captcha: captchaResponse });
  }

  captchaResult() {
    return this.authService.captchaResult();
  }

  register(): void {
    
    if(this.form.invalid) {
      return;
    }

    if(this.form.value.password !== this.form.value.passwordConfirm) {
      return;
    }

    const userData = {
      firstname: this.form.value.firstname,
      lastname: this.form.value.lastname,
      image: this.form.value.image,
      username: this.form.value.username,
      password: this.form.value.password,
      birthdate: this.form.value.birthdate,
      country: this.form.value.country,
      city: this.form.value.city,
      email: this.form.value.email
    };

    this.authService.registerUser(userData);

  }

}
