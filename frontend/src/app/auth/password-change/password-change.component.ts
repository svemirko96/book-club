import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent implements OnInit {

  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  msg: string;

  constructor(
    private authService: AuthService) { }

  ngOnInit(): void {
  }

  changePassword() {
    if(this.newPassword !== this.confirmPassword)
      return;

    const passwordData = {
      userId: localStorage.getItem('userId'),
      oldPassword: this.oldPassword,
      newPassword: this.newPassword
    };
    this.authService.changePassword(passwordData);
    this.authService.logout();
  }

}
