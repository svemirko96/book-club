import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { BookSearchComponent } from './books/book-search/book-search.component';
import { BookDetailsComponent } from './books/book-details/book-details.component';
import { ProfilePageComponent } from './user/profile-page/profile-page.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { BookAddComponent } from './books/book-add/book-add.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PasswordChangeComponent } from './auth/password-change/password-change.component';
import { HeaderComponent } from './header/header.component';
import { UserCommentsComponent } from './user/user-comments/user-comments.component';
import { ReadingOverviewComponent } from './user/reading-overview/reading-overview.component';
import { UserSearchComponent } from './user/user-search/user-search.component';
import { GuestPageComponent } from './user/guest-page/guest-page.component';
import { EventsOverviewComponent } from './events/events-overview/events-overview.component';
import { VerifyBooksComponent } from './admin/verify-books/verify-books.component';
import { VerifyUsersComponent } from './admin/verify-users/verify-users.component';
import { GenreAddComponent } from './admin/genre-add/genre-add.component';
import { GenreDeleteComponent } from './admin/genre-delete/genre-delete.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { BookEditComponent } from './books/book-edit/book-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    BookSearchComponent,
    BookDetailsComponent,
    ProfilePageComponent,
    UserDetailsComponent,
    BookAddComponent,
    PasswordChangeComponent,
    HeaderComponent,
    UserCommentsComponent,
    ReadingOverviewComponent,
    UserSearchComponent,
    GuestPageComponent,
    EventsOverviewComponent,
    VerifyBooksComponent,
    VerifyUsersComponent,
    GenreAddComponent,
    GenreDeleteComponent,
    UserEditComponent,
    BookEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
