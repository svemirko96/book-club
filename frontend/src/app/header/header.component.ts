import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  userId: any;
  user: any;
  isAuthenticated: boolean;

  authListenerSub: Subscription;
  userSub: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isAuthenticated = this.authService.getIsAuthenticated();
    if(this.isAuthenticated) this.userId = localStorage.getItem('userId');
    this.authListenerSub = this.authService.getAuthStatusListener()
      .subscribe(isAuth => {
        this.userId = this.authService.getUserId();
        this.isAuthenticated = isAuth;
        if(this.isAuthenticated) this.authService.getLoggedInUser();
      });

    this.userSub = this.authService.getLoggedInUserUpdateListener()
      .subscribe(user => {
        this.user = user;
        //console.log(user);
      });
    if(this.isAuthenticated) {
      this.authService.getLoggedInUser();
    }

  }

  logout() {
    this.authService.logout();
  }

  isMod() {
    return this.user.type == 'mod' || this.user.type == 'admin';
  }

  isAdmin() {
    return this.user.type == 'admin';
  }

  ngOnDestroy() {
    this.authListenerSub.unsubscribe();
    this.userSub.unsubscribe();
  }

}
