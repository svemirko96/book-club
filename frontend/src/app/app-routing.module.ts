import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ProfilePageComponent } from './user/profile-page/profile-page.component';
import { BookDetailsComponent } from './books/book-details/book-details.component';
import { PasswordChangeComponent } from './auth/password-change/password-change.component';
import { BookAddComponent } from './books/book-add/book-add.component';
import { VerifyBooksComponent } from './admin/verify-books/verify-books.component';
import { VerifyUsersComponent } from './admin/verify-users/verify-users.component';
import { UserSearchComponent } from './user/user-search/user-search.component';
import { GenreAddComponent } from './admin/genre-add/genre-add.component';
import { GenreDeleteComponent } from './admin/genre-delete/genre-delete.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { GuestPageComponent } from './user/guest-page/guest-page.component';
import { BookEditComponent } from './books/book-edit/book-edit.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile-page/:id', component: ProfilePageComponent },
  { path: 'book/:id', component: BookDetailsComponent },
  { path: 'new-password', component: PasswordChangeComponent },
  { path: 'new-book', component: BookAddComponent },
  { path: 'books-verification', component: VerifyBooksComponent },
  { path: 'user-management', component: VerifyUsersComponent },
  { path: 'user-search', component: UserSearchComponent },
  { path: 'new-genre', component: GenreAddComponent },
  { path: 'delete-genre', component: GenreDeleteComponent },
  { path: 'edit-user', component: UserEditComponent },
  { path: 'guest-page', component: GuestPageComponent },
  { path: 'edit-book/:id', component: BookEditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
