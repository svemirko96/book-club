import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { BookEventsService } from '../../services/book-events.service';

@Component({
  selector: 'app-events-overview',
  templateUrl: './events-overview.component.html',
  styleUrls: ['./events-overview.component.css']
})
export class EventsOverviewComponent implements OnInit, OnDestroy {

  events: any[];
  eventsSub: Subscription;

  constructor(private eventsService: BookEventsService) { }

  ngOnInit(): void {

    this.eventsSub = this.eventsService.getAllBookEventsUpdateListener()
      .subscribe(fetchedEvents => {
        this.events = [];
        fetchedEvents.forEach(e => {
          if(new Date(e.endDate) >= new Date() && e.public) {
            this.events.push(e);
          }
        });
      });
    this.eventsService.getAllBookEvents();

  }

  formatStartDate(d) {
    if(new Date(d) > new Date()) {
      let dd = new Date(d);
      return dd.getDate() + '.' + (dd.getMonth() + 1) + '.' + dd.getFullYear() + '.';
    } else {
      return 'Aktivan';
    }
  }

  ngOnDestroy() {
    this.eventsSub.unsubscribe();
  }

}
