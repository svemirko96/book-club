import { Component, OnInit, OnDestroy } from '@angular/core';

import { UsersService } from '../../services/users.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit, OnDestroy {

  user: any;
  private currentUserSub: Subscription;

  constructor(private usersService: UsersService,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {

    let userId = this.route.snapshot.paramMap.get('id');
    this.usersService.getUserById(userId);
    this.currentUserSub = this.usersService.getUserUpdateListener()
      .subscribe((user: any) => {
        this.user = user;
      });

  }

  formatDate(dstr) {
    let d = new Date(dstr);
    return d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear() + '.';
  }

  ngOnDestroy() {
    this.currentUserSub.unsubscribe();
  }

}
