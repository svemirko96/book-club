import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent implements OnInit, OnDestroy {

  form: FormGroup;

  queriedUsers: any[];
  queriedUsersSub: Subscription;

  queryDone: boolean = false;

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      firstname: new FormControl(null, {
        validators: []
      }),
      lastname: new FormControl(null, {
        validators: []
      }),
      username: new FormControl(null, {
        validators: []
      }),
      email: new FormControl(null, {
        validators: []
      })
    });

    this.queriedUsersSub = this.usersService.getQueriedUsersUpdateListener()
      .subscribe(users => {
        this.queriedUsers = users;
        this.queryDone = true;
      });

  }

  searchUsers() {
    this.queryDone = false;
    let queryParams = '';
    if(this.form.value.firstname !== null && this.form.value.firstname !== '') {
      queryParams += `firstname=${this.form.value.firstname}`;
    }
    if(this.form.value.lastname !== null && this.form.value.lastname !== '') {
      if(queryParams.length > 0) queryParams += '&';
      queryParams += `lastname=${this.form.value.lastname}`;
    }
    if(this.form.value.username !== null && this.form.value.username !== '') {
      if(queryParams.length > 0) queryParams += '&';
      queryParams += `username=${this.form.value.username}`;
    }
    if(this.form.value.email !== null && this.form.value.email !== '') {
      if(queryParams.length > 0) queryParams += '&';
      queryParams += `email=${this.form.value.email}`;
    }
    if(queryParams.length > 0) queryParams = '?' + queryParams;
    else return;
    console.log(queryParams);
    this.usersService.getUsers(queryParams);
  }

  ngOnDestroy() {
    this.queriedUsersSub.unsubscribe();
  }

}
