import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  form: FormGroup;

  user: any;
  userSub: Subscription;

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private router: Router) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      firstname: new FormControl(null, {
          validators: []
      }),
      lastname: new FormControl(null, {
        validators: []
      }),
      image: new FormControl(null, {
        validators: []
      }),
      birthdate: new FormControl(null, {
        validators: []
      }),
      country: new FormControl(null, {
        validators: []
      }),
      city: new FormControl(null, {
        validators: []
      }),
      email: new FormControl(null, {
        validators: [Validators.email]
      })
    });

    let userId = this.authService.getUserId();
    this.userSub = this.usersService.getUserUpdateListener()
      .subscribe(user => {
        this.user = user;
        this.form.patchValue({firstname : user.firstname});
        this.form.patchValue({lastname : user.lastname});
        this.form.patchValue({birthdate : user.birthdate});
        this.form.patchValue({country : user.country});
        this.form.patchValue({city : user.city});
        this.form.patchValue({email : user.email});
      });
    this.usersService.getUserById(userId);

  }

  onImagePicked(files: File[]) {
    // console.log(files[0]);
    this.form.patchValue({image: files[0]});
    this.form.get('image').updateValueAndValidity();
  }

  editUserData() {
    const formData = new FormData();
    formData.append('userId', this.user._id);
    formData.append('firstname', this.form.value.firstname);
    formData.append('lastname', this.form.value.lastname);
    formData.append('birthdate', this.form.value.birthdate);
    formData.append('country', this.form.value.country);
    formData.append('city', this.form.value.city);
    formData.append('email', this.form.value.email);
    if(this.form.value.image != null)
      formData.append('image', this.form.value.image, this.form.value.username);
    this.usersService.updateUserData(formData);
    this.router.navigate(['/profile-page', this.user._id]);
  }

}
