import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadingOverviewComponent } from './reading-overview.component';

describe('ReadingOverviewComponent', () => {
  let component: ReadingOverviewComponent;
  let fixture: ComponentFixture<ReadingOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadingOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadingOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
