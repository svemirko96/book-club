import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { BooksService } from 'src/app/services/books.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-reading-overview',
  templateUrl: './reading-overview.component.html',
  styleUrls: ['./reading-overview.component.css']
})
export class ReadingOverviewComponent implements OnInit, OnDestroy {

  active = 1;
  page = 0;
  pageSize = 4;

  user: any;
  userSub: Subscription;

  books: any[];
  booksSub: Subscription;

  finishedBooks: any[] = [];
  activeBooks: any[] = [];
  toReadBooks: any[] = [];

  constructor(
    private usersService: UsersService,
    private booksService: BooksService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.userSub = this.usersService.getUserUpdateListener()
      .subscribe(user => {
        this.user = user;
        this.finishedBooks = [];
        this.activeBooks = [];
        this.toReadBooks = [];
        for(let i = 0; i < user.books.length; i++) {
          if(user.books[i].status == 'finished')
            this.finishedBooks.push(user.books[i]);
          else if(user.books[i].status == 'active')
            this.activeBooks.push(user.books[i]);
          else
            this.toReadBooks.push(user.books[i]);
        }
      });
    this.usersService.getUserById(this.route.snapshot.paramMap.get('id'));

    this.booksSub = this.booksService.getAllBooksUpdateListener()
      .subscribe(books => {
        this.books = books;
      });
    this.booksService.getAllBooks();

  }

  changePageSize(target) {
    this.pageSize = target.valueAsNumber;
  }

  bkRemoveFromList(bookId) {
    const actionData = {
      userId: this.user._id,
      bookId: bookId,
      action: 'removeFromList'
    }
    this.usersService.updateBooksList(actionData);
    for(let i = 0; i < this.toReadBooks.length; i++) {
      if(this.toReadBooks[i].book == bookId) {
        this.toReadBooks.splice(i, 1);
        break;
      }
    }
  }

  bookName(bookId) {
    for(let i = 0; i < this.books.length; i++) {
      if(this.books[i]._id == bookId) {
        return this.books[i].title;
      }
    }
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
    this.booksSub.unsubscribe();
  }

}
