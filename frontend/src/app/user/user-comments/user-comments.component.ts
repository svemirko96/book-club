import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-user-comments',
  templateUrl: './user-comments.component.html',
  styleUrls: ['./user-comments.component.css']
})
export class UserCommentsComponent implements OnInit, OnDestroy {

  books: any[];
  booksSub: Subscription;

  ratings: any[];
  ratingsSub: Subscription;

  constructor(private booksService: BooksService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.booksSub = this.booksService.getAllBooksUpdateListener()
      .subscribe(books => {
        this.books = books;
      });
    this.booksService.getAllBooks();

    let userId = this.route.snapshot.paramMap.get('id');
    this.ratingsSub = this.booksService.getRatingsByUserUpdateListener()
      .subscribe(data => {
        this.ratings = data.ratings;
      });
    this.booksService.getRatingsByUser(userId);

  }

  bookName(bookId): string {
    for(let i = 0; i < this.books.length; i++) {
      if(this.books[i]._id == bookId) {
        return this.books[i].title;
      }
    }
    return '';
  }

  bookAuthors(bookId): string {
    let authors = '';
    for(let i = 0; i < this.books.length; i++) {
      if(this.books[i]._id == bookId) {
        for(let j = 0; j < this.books[i].authors.length; j++) {
          if(authors.length > 0) authors += ', ';
          authors += this.books[i].authors[j];
        }
        break;
      }
    }
    return authors;
  }

  ngOnDestroy(): void {
    this.booksSub.unsubscribe();
    this.ratingsSub.unsubscribe();
  }

}
