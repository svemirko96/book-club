import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  viewingLoggedInUser = false;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.viewingLoggedInUser = localStorage.getItem('userId') == this.route.snapshot.paramMap.get('id');
  }

}
