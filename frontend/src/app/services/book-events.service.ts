import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BookEventsService {

    private allBookEvents = new Subject<any[]>();

    constructor(private http: HttpClient) {

    }

    getAllBookEventsUpdateListener() {
        return this.allBookEvents.asObservable();
    }

    getAllBookEvents() {
        this.http.get<any[]>('http://localhost:3000/api/books/events')
            .subscribe(events => {
                this.allBookEvents.next(events);
            })
    }

}