import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    private userUpdated = new Subject<any>();
    private allUsersUpdated = new Subject<{ users: any[] }>();
    private queriedUsersUpdated = new Subject<any[]>();

    constructor(private http: HttpClient, private router: Router) {

    }

    // ---------------- Update Listeners -----------------

    getUserUpdateListener() {
        return this.userUpdated.asObservable();
    }

    getAllUsersUpdateListener() {
        return this.allUsersUpdated.asObservable();;
    }

    getQueriedUsersUpdateListener() {
        return this.queriedUsersUpdated.asObservable();
    }

    // ----------------- Service functions -------------------

    getUserById(userId: string) {
        this.http.get('http://localhost:3000/api/user/single/' + userId)
            .subscribe(user => {
                this.userUpdated.next(user);
            });
    }

    getAllUsers() {
        this.http.get<{ users: any[] }>('http://localhost:3000/api/user/all-users')
            .subscribe(users => {
                this.allUsersUpdated.next(users);
            });
    }

    getUsers(queryParams) {
        this.http.get<any[]>('http://localhost:3000/api/user/search' + queryParams)
            .subscribe(users => {
                this.queriedUsersUpdated.next(users);
            });
    }

    updateUserData(formData) {
        this.http.post('http://localhost:3000/api/user/edit-user', formData)
            .subscribe(res => {
                // console.log('Book successfuly set to finished');
            });
    }

    updateBooksList(actionData) {
        this.http.post('http://localhost:3000/api/user/update-books', actionData)
            .subscribe(res => {
                // console.log('Book successfuly set to finished');
            });
    }

    resolveRegistrationRequest(requestAction) {
        this.http.post('http://localhost:3000/api/user/user-management', requestAction)
            .subscribe(res => {
                // console.log('Book successfuly set to finished');
            });
    }

}