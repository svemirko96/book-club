import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BooksService {

    private genresUpdated = new Subject<{ genres: any[] }>();
    private booksUpdated = new Subject<{ books: any[] }>();
    private bookUpdated = new Subject<{ book: any }>();
    private allBooksUpdated = new Subject<any[]>();
    private newBookUpdated = new Subject<any>();

    private ratingsForBookUpdated = new Subject<{ ratings: any[] }>();
    private ratingsByUserUpdated = new Subject<{ ratings: any[] }>();

    private newGenreUpdated = new Subject<any>();

    constructor(private http: HttpClient, private router: Router) {

    }

    // ---------------- Update Listeners --------------------

    getGenreUpdateListener() {
        return this.genresUpdated.asObservable();
    }

    getBookUpdateListener() {
        return this.booksUpdated.asObservable();
    }

    getSingleBookUpdateListener() {
        return this.bookUpdated.asObservable();
    }

    getAllBooksUpdateListener() {
        return this.allBooksUpdated.asObservable();
    }

    getNewBookUpdateListener() {
        return this.newBookUpdated.asObservable();
    }

    getRatingsForBookUpdatedListener() {
        return this.ratingsForBookUpdated.asObservable();
    }

    getRatingsByUserUpdateListener() {
        return this.ratingsByUserUpdated.asObservable();
    }

    getNewGenreUpdateListener() {
        return this.newGenreUpdated.asObservable();
    }

    // -------------------- Service functions ----------------------

    getAllGenres() {
        this.http.get<{ genres: any[] }>('http://localhost:3000/api/books/genres')
            .subscribe(data => {
                this.genresUpdated.next({ genres: data.genres });
            });
    }

    getBookById(id) {
        this.http.get<any>('http://localhost:3000/api/books/book/' + id)
            .subscribe(book => {
                this.bookUpdated.next(book);
            });
    }

    getAllBooks() {
        this.http.get<any[]>('http://localhost:3000/api/books/all-books')
            .subscribe(data => {
                this.allBooksUpdated.next(data);
            });
    }

    getBooks(queryParams) {
        this.http.get<{ books: any[] }>('http://localhost:3000/api/books/search' + queryParams)
            .subscribe(data => {
                this.booksUpdated.next({ books: data.books });
            });
    }

    addBook(bookData) {
        const formData = new FormData();
        formData.append('title', bookData.title);
        formData.append('authors', bookData.authors);
        if(bookData.image != null)
            formData.append('image', bookData.image, bookData.title);
        formData.append('publishDate', bookData.publishDate);
        formData.append('genres', bookData.genres);
        formData.append('description', bookData.description);
        this.http.post('http://localhost:3000/api/books/new-book', formData)
            .subscribe(res => {
                this.newBookUpdated.next(res);
            });
    }
    
    updateBookData(formData) {
        this.http.post('http://localhost:3000/api/books/edit-book', formData)
            .subscribe(res => {
                // console.log('Uspesno izmenjeni podaci knjige');
            })
    }

    verifyBook(verifyData) {
        this.http.post('http://localhost:3000/api/books/verify-book', verifyData)
            .subscribe(res => {
                console.log('Uspesno odobrena knjiga');
            });
    }

    leaveRating(ratingData) {
        this.http.post('http://localhost:3000/api/books/book-rating', ratingData)
            .subscribe(res => {
                console.log('Uspesno ostavljena ocena');
            });
    }

    getBookRatings(bookId) {
        this.http.get<any[]>('http://localhost:3000/api/books/book-rating/' + bookId)
            .subscribe(data => {
                this.ratingsForBookUpdated.next({ ratings: data });
            });
    }

    getRatingsByUser(userId) {
        this.http.get<any[]>('http://localhost:3000/api/books/book-ratings-by-user/' + userId)
            .subscribe(data => {
                this.ratingsByUserUpdated.next({ ratings: data });
            });
    }

    addNewGenre(genreData) {
        this.http.post('http://localhost:3000/api/books/new-genre', genreData)
            .subscribe(response => {
                this.newGenreUpdated.next(response);
            });
    }

    deleteGenre(genreData) {
        this.http.post('http://localhost:3000/api/books/delete-genre', genreData)
            .subscribe(response => {
                // console.log(response);
            });
    }

}