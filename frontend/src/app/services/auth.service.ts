import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { UsersService } from './users.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private userId: string;
    private token: string;
    private loggedInUserUpdated = new Subject<any>();

    private isAuthenticated: boolean = false;
    private isAuthenticatedUpdated = new Subject<boolean>();

    private captchaPassed: boolean = false;

    constructor(
        private http: HttpClient,
        private router: Router) {

    }
    
    // ---------------- Update Listeners -----------------

    getLoggedInUserUpdateListener() {
        return this.loggedInUserUpdated.asObservable();
    }

    getAuthStatusListener() {
        return this.isAuthenticatedUpdated.asObservable();
    }

    // ------------------ Service functions ---------------

    getUserId() {
        return this.userId;
    }

    getIsAuthenticated() {
        return this.isAuthenticated;
    }

    captchaResult() {
        return this.captchaPassed;
    }

    autoAuthenticate() {
        if(localStorage.getItem('userId') != null) {
            this.userId = localStorage.getItem('userId');
            this.token = localStorage.getItem('token');
            this.isAuthenticated = true;
            this.isAuthenticatedUpdated.next(true);
            //this.router.navigate(['/profile-page', this.userId]);
        }
    }

    getLoggedInUser() {
        if(this.isAuthenticated && this.userId) {
            this.http.get<any>('http://localhost:3000/api/user/single/' + this.userId)
                .subscribe(user => {
                    this.loggedInUserUpdated.next(user);
                });
        }
    }

    checkCaptcha(captchaResponse) {
        this.http.post<{ success: boolean, msg: string }>('http://localhost:3000/api/user/check-captcha', captchaResponse)
            .subscribe(data => {
                //console.log(data);
                if(data.success == true) {
                    this.captchaPassed = true;
                }
            });
    }

    registerUser(userData) {
        if(!this.captchaPassed) {
            return;
        }
        this.captchaPassed = false;
        const formData = new FormData();
        formData.append('firstname', userData.firstname);
        formData.append('lastname', userData.lastname);
        formData.append('username', userData.username);
        formData.append('password', userData.password);
        formData.append('birthdate', userData.birthdate);
        formData.append('country', userData.country);
        formData.append('city', userData.city);
        formData.append('email', userData.email);
        if(userData.image != null)
            formData.append('image', userData.image, userData.username);
        this.http.post('http://localhost:3000/api/user/register', formData)
            .subscribe(res => {
                console.log(res);
            });
        this.router.navigate(['/']);
    }

    login(loginData) {
        this.http.post<{ message: string, token: string, expiresIn: number, userId: string }>('http://localhost:3000/api/user/login', loginData)
            .subscribe(data => {
                if(data.message == 'success') {
                    this.userId = data.userId;
                    this.token = data.token;
                    this.isAuthenticated = true;
                    this.isAuthenticatedUpdated.next(true);
                    localStorage.setItem('userId', this.userId);
                    localStorage.setItem('token', this.token);
                    this.router.navigate(['profile-page', this.userId]);
                } else if(data.message == 'userNotFound') {
                    console.log('Korisnik sa navedenim imenom ne postoji u sistemu');
                } else if(data.message == 'invalidPassword') {
                    console.log('Lozinka nije ispravna');
                }
                
            });
    }

    changePassword(passwordData) {
        this.http.post('http://localhost:3000/api/user/new-password', passwordData)
            .subscribe(res => {
                //console.log(res);
                this.logout();
            });
    }

    logout() {
        this.userId = null;
        this.token = null;
        this.isAuthenticated = false;
        this.isAuthenticatedUpdated.next(false);
        localStorage.removeItem('userId');
        localStorage.removeItem('token');
        this.router.navigate(['/']);
    }

}