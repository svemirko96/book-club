import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit, OnDestroy {

  isAuthenticated: boolean;

  genres: any[];
  genresSub: Subscription;

  book: any;
  bookSub: Subscription;

  user: any;
  userSub: Subscription;

  rStatus: string;
  pagesRead: number;
  pagesCount: number;
  percentRead: number;

  rating: number = 0;
  comment: string;

  bookRatings: any[];
  bookRatingsSub: Subscription;
  ratingsFetched = false;
  avgRating: number;

  allUsers: any[];
  allUsersSub: Subscription;


  constructor(
    private booksService: BooksService,
    private usersService: UsersService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.isAuthenticated = localStorage.getItem('userId') !== null;

    this.booksService.getAllGenres();
    this.genresSub = this.booksService.getGenreUpdateListener()
      .subscribe((genresData: { genres: any[] }) => {
        this.genres = genresData.genres;
      });

    this.booksService.getBookById(this.route.snapshot.paramMap.get('id'));
    this.bookSub = this.booksService.getSingleBookUpdateListener()
      .subscribe((book: any) => {
        this.book = book;
        if(this.user) this.updateReadingStatus();
        this.booksService.getBookRatings(book._id);
      });

    if(this.isAuthenticated) {
      this.usersService.getUserById(localStorage.getItem('userId'));
      this.userSub = this.usersService.getUserUpdateListener()
        .subscribe((user) => {
          this.user = user;
          if(this.book) this.updateReadingStatus();
        });
    }

    this.bookRatingsSub = this.booksService.getRatingsForBookUpdatedListener()
      .subscribe((data) => {
        this.bookRatings = data.ratings;
        if(this.allUsers) this.ratingsFetched = true;
        let sum = 0, n = 0;
        for(let i = 0; i < this.bookRatings.length; i++) {
          if(this.user && this.bookRatings[i].userId == this.user._id) {
            this.rating = this.bookRatings[i].rating;
            this.comment = this.bookRatings[i].comment;
          }
          sum += this.bookRatings[i].rating;
          n += 1
        }
        if(n > 0) this.avgRating = sum / n;
        else this.avgRating = -1;
      });

    this.allUsersSub = this.usersService.getAllUsersUpdateListener()
      .subscribe((data) => {
        this.allUsers = data.users;
        if(this.bookRatings) this.ratingsFetched = true;
      });
    this.usersService.getAllUsers();

  }

  leaveRating() {
    if(this.rating == 0 || (this.percentRead < 0.5 && this.rStatus != 'finished'))
      return;
    const ratingData = {
      userId: this.user._id,
      bookId: this.book._id,
      rating: this.rating,
      comment: this.comment
    };
    this.booksService.leaveRating(ratingData);
    let isNewComment = true;
    for(let i = 0; i < this.bookRatings.length; i++) {
      if(this.bookRatings[i].userId == this.user._id) {
        this.bookRatings[i].rating = this.rating;
        this.bookRatings[i].comment = this.comment;
        isNewComment = false;
        break;
      }
    }
    if(isNewComment) this.bookRatings.push(ratingData);
  }

  bkAddToFinished() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'addToFinished'
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'finished';
  }

  bkAddToList() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'addToList'
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'toRead';
  }

  bkAddToCurrentlyRead() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'addToCurrentlyRead'
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'active';
  }

  bkStartReading() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'startReading'
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'active';
  }

  bkFinishReading() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'finishReading'
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'finished';
  }

  bkUpdatePages() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'updatePages',
      pagesRead: this.pagesRead,
      pagesCount: this.pagesCount
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'active';
    this.percentRead = this.pagesRead / this.pagesCount;
  }

  bkRemoveFromList() {
    const actionData = {
      userId: this.user._id,
      bookId: this.book._id,
      action: 'removeFromList'
    }
    this.usersService.updateBooksList(actionData);
    this.rStatus = 'notFound';
  }

  formatUsersFullName(userId): string {
    for(let i = 0; i < this.allUsers.length; i++) {
      if(this.allUsers[i]._id == userId)
        return this.allUsers[i].firstname + ' ' + this.allUsers[i].lastname;
    }
    return;
  }

  formatDate(dstr) {
    let d = new Date(dstr);
    return d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear() + '.';
  }

  formatAuthors(b) {
    let authors = '';
    b.authors.forEach(a => {
      if(authors.length > 0) authors += ', '
      authors += a;
    });
    return authors;
  }

  formatGenres(b) {
    let genres = '';
    b.genres.forEach(g => {
      if(genres.length > 0) genres += ', '
      genres += this.getGenreName(g);
    });
    return genres;
  }

  getGenreName(id: string) {
    for(let i = 0; i < this.genres.length; i++)
      if(this.genres[i]._id == id)
        return this.genres[i].name;
  }

  updateReadingStatus() {
    for(let i = 0; i < this.user.books.length; i++) {
      if(this.user.books[i].book == this.book._id) {
        this.rStatus = this.user.books[i].status;
        this.pagesRead = this.user.books[i].pagesRead;
        this.pagesCount = this.user.books[i].pagesCount;
        this.percentRead = this.pagesRead / this.pagesCount;
        return;
      }
    }
    this.rStatus = 'notFound';
  }

  ngOnDestroy() {
    this.genresSub.unsubscribe();
    this.bookSub.unsubscribe();
    if(this.userSub) this.userSub.unsubscribe();
    this.bookRatingsSub.unsubscribe();
    this.allUsersSub.unsubscribe();
  }

}
