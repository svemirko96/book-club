import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit, OnDestroy {

  form: FormGroup;

  genres: any[];
  genresSub: Subscription;

  bookAddedSub: Subscription;
  message: string;
  success: boolean;

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {
    
    this.booksService.getAllGenres();
    this.genresSub = this.booksService.getGenreUpdateListener()
      .subscribe((genresData: { genres: any[] }) => {
        this.genres = genresData.genres;
      });
    
    this.bookAddedSub = this.booksService.getNewBookUpdateListener()
      .subscribe(result => {
        if(result.message == 'success') {
          this.success = true;
          this.message = 'Uspešno dodata knjiga.';
          this.form.reset();
        } else {
          this.success = false;
          this.message = 'Dodavanje knjige nije uspelo.';
        }
      });

    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required],
      }),
      image: new FormControl(null, {
        validators: [],
      }),
      authors: new FormControl(null, {
        validators: [Validators.required],
      }),
      publishDate: new FormControl(null, {
        validators: [Validators.required],
      }),
      genres: new FormControl(null, {
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        validators: [Validators.required],
      })
    });

  }

  onImagePicked(files: File[]) {
    // console.log(files[0]);
    this.form.patchValue({image: files[0]});
    this.form.get('image').updateValueAndValidity();
  }

  addBook() {

    const bookData = {
      title: this.form.value.title,
      image: this.form.value.image,
      authors: this.form.value.authors,
      publishDate: this.form.value.publishDate,
      genres: this.form.value.genres.slice(0,3),
      description: this.form.value.description
    }
    console.log(bookData);
    this.booksService.addBook(bookData);
  }

  ngOnDestroy() {
    this.genresSub.unsubscribe();
    this.bookAddedSub.unsubscribe();
  }

}
