import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  form: FormGroup;

  book: any;
  bookSub: Subscription;

  genres: any[];
  genresSub: Subscription;

  loaded: boolean;

  constructor(
    private booksService: BooksService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required],
      }),
      image: new FormControl(null, {
        validators: [],
      }),
      authors: new FormControl(null, {
        validators: [Validators.required],
      }),
      publishDate: new FormControl(null, {
        validators: [Validators.required],
      }),
      genres: new FormControl(null, {
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        validators: [Validators.required],
      })
    });

    let bookId = this.route.snapshot.paramMap.get('id');
    this.bookSub = this.booksService.getSingleBookUpdateListener()
      .subscribe(book => {
        this.book = book;
        this.form.patchValue({ title: this.book.title });
        let authorsStr = '';
        for(let i = 0; i < this.book.authors.length; i++) {
          if(authorsStr.length > 0) authorsStr += ', ';
          authorsStr += this.book.authors[i];
        }
        this.form.patchValue({ authors: authorsStr });
        this.form.patchValue({ publishDate: this.book.publishDate });
        //this.form.patchValue({ genres: this.book.genres });
        this.form.patchValue({ description: this.book.description });
        if(this.genres) this.loaded = true;
      });
    this.booksService.getBookById(bookId);
    
    this.genresSub = this.booksService.getGenreUpdateListener()
      .subscribe(data => {
        this.genres = data.genres;
        if(this.book) this.loaded = true;
      });
    this.booksService.getAllGenres();

  }

  onImagePicked(files: File[]) {
    // console.log(files[0]);
    this.form.patchValue({image: files[0]});
    this.form.get('image').updateValueAndValidity();
  }

  editBookData() {
    const formData = new FormData();
    formData.append('bookId', this.book._id);
    formData.append('title', this.form.value.title);
    formData.append('authors', this.form.value.authors);
    formData.append('publishDate', this.form.value.publishDate);
    formData.append('genres', this.form.value.genres);
    formData.append('description', this.form.value.description);
    if(this.form.value.image != null)
      formData.append('image', this.form.value.image, this.form.value.title);
    this.booksService.updateBookData(formData);
    this.router.navigate(['/book', this.book._id]);
  }

}
