import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

import {BooksService } from '../../services/books.service';

@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.css']
})
export class BookSearchComponent implements OnInit, OnDestroy {

  form: FormGroup;

  genres: any[];
  genresSub: Subscription;

  queriedBooks: any[];
  booksSub: Subscription;
  queryDone: boolean = false;

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {

    this.booksService.getAllGenres();
    this.genresSub = this.booksService.getGenreUpdateListener()
      .subscribe((genresData: { genres: any[] }) => {
        this.genres = genresData.genres;
      });

    this.booksSub = this.booksService.getBookUpdateListener()
      .subscribe((booksData: { books: any[] }) => {
        this.queriedBooks = booksData.books;
        this.queryDone = true;
        // console.log(this.queriedBooks);
      });

    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: []
      }),
      author: new FormControl(null, {
        validators: []
      }),
      genre: new FormControl(null, {
        validators: []
      }),
    });

  }

  searchBooks() {
    this.queryDone = false;
    let queryParams = '';
    if(this.form.value.title !== null && this.form.value.title !== '') {
      queryParams += `title=${this.form.value.title}`;
    }
    if(this.form.value.author !== null && this.form.value.author !== '') {
      if(queryParams.length > 0) queryParams += '&';
      queryParams += `author=${this.form.value.author}`;
    }
    if(this.form.value.genre !== null && this.form.value.genre !== '') {
      if(queryParams.length > 0) queryParams += '&';
      queryParams += `genre=${this.form.value.genre}`;
    }
    if(queryParams.length > 0) queryParams = '?' + queryParams;
    else return;
    this.booksService.getBooks(queryParams);
  }

  formatAuthors(b) {
    let authors = '';
    b.authors.forEach(a => {
      if(authors.length > 0) authors += ', '
      authors += a;
    });
    return authors;
  }

  formatGenres(b) {
    let genres = '';
    b.genres.forEach(g => {
      if(genres.length > 0) genres += ', '
      genres += this.getGenreName(g);
    });
    return genres;
  }

  getGenreName(id: string) {
    for(let i = 0; i < this.genres.length; i++)
      if(this.genres[i]._id == id)
        return this.genres[i].name;
  }

  ngOnDestroy() {
    this.genresSub.unsubscribe();
    this.booksSub.unsubscribe();
  }

}
