import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-verify-users',
  templateUrl: './verify-users.component.html',
  styleUrls: ['./verify-users.component.css']
})
export class VerifyUsersComponent implements OnInit, OnDestroy {

  unregisteredUsers: any[];
  regularUsers: any[];
  moderators: any[];
  usersSub: Subscription;

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {

    this.usersSub = this.usersService.getAllUsersUpdateListener()
      .subscribe(data => {
        this.unregisteredUsers = [];
        this.regularUsers = [];
        this.moderators = [];
        data.users.forEach(u => {
          if(u.type == 'unverified') this.unregisteredUsers.push(u);
          else if(u.type == 'user') this.regularUsers.push(u);
          else if(u.type == 'mod') this.moderators.push(u);
        });
        // console.log(this.unregisteredUsers);
        // console.log(this.regularUsers);
        // console.log(this.moderators);
      });
    this.usersService.getAllUsers();

  }

  acceptRegistration(userId, i) {
    const requestAction = {
      userId: userId,
      action: 'accept'
    };
    this.usersService.resolveRegistrationRequest(requestAction);
    this.regularUsers.push(this.unregisteredUsers[i]);
    this.unregisteredUsers.splice(i, 1);
  }

  rejectRegistration(userId, i) {
    const requestAction = {
      userId: userId,
      action: 'reject'
    };
    this.usersService.resolveRegistrationRequest(requestAction);
    this.unregisteredUsers.splice(i, 1);
  }

  promoteUser(userId, i) {
    const requestAction = {
      userId: userId,
      action: 'promote'
    };
    this.usersService.resolveRegistrationRequest(requestAction);
    this.moderators.push(this.regularUsers[i]);
    this.regularUsers.splice(i, 1);
  }

  demoteUser(userId, i) {
    const requestAction = {
      userId: userId,
      action: 'demote'
    };
    this.usersService.resolveRegistrationRequest(requestAction);
    this.regularUsers.push(this.moderators[i]);
    this.moderators.splice(i, 1);
  }

  ngOnDestroy() {
    this.usersSub.unsubscribe();
  }

}
