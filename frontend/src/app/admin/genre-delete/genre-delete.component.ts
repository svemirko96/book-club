import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-genre-delete',
  templateUrl: './genre-delete.component.html',
  styleUrls: ['./genre-delete.component.css']
})
export class GenreDeleteComponent implements OnInit {

  genreId;
  genres: any[];
  genresSub: Subscription;

  books: any[];
  booksSub: Subscription;

  loaded: boolean = false;
  status: string;

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {

    this.genresSub = this.booksService.getGenreUpdateListener()
      .subscribe(data => {
        this.genres = data.genres;
        if(this.books) this.loaded = true;
      })
    this.booksService.getAllGenres();

    this.booksSub = this.booksService.getAllBooksUpdateListener()
      .subscribe(books => {
        this.books = books;
        if(this.genres) this.loaded = true;
      })
    this.booksService.getAllBooks();

  }

  deleteGenre() {

    let inUse = false;
    this.books.forEach(b => {
      b.genres.forEach(g => {
        if(g == this.genreId)
          inUse = true;
      });
    });

    if(inUse) {
      this.status = 'error';
    } else {
      this.booksService.deleteGenre({ genreId: this.genreId });
      this.status = 'success';
      for(let i = 0; i < this.genres.length; i++) {
        if(this.genreId == this.genres[i]._id) {
          this.genres.splice(i, 1);
          break;
        }
      }
    }
    

  }

}
