import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-verify-books',
  templateUrl: './verify-books.component.html',
  styleUrls: ['./verify-books.component.css']
})
export class VerifyBooksComponent implements OnInit, OnDestroy {

  unverifiedBooks: any[];
  booksSub: Subscription;

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {

    this.unverifiedBooks = [];
    this.booksSub = this.booksService.getAllBooksUpdateListener()
      .subscribe(allBooks => {
        this.unverifiedBooks = [];
        allBooks.forEach(b => {
          if(!b.accepted) {
            this.unverifiedBooks.push(b);
          }
        });
      });
    this.booksService.getAllBooks();

  }

  verifyBook(bookId, index) {
    const verifyData = {
      bookId: bookId
    }
    this.booksService.verifyBook(verifyData);
    this.unverifiedBooks.splice(index, 1);
  }

  ngOnDestroy() {
    this.booksSub.unsubscribe();
  }

}
