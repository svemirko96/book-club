import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyBooksComponent } from './verify-books.component';

describe('VerifyBooksComponent', () => {
  let component: VerifyBooksComponent;
  let fixture: ComponentFixture<VerifyBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyBooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
