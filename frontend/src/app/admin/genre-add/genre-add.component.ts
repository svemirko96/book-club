import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-genre-add',
  templateUrl: './genre-add.component.html',
  styleUrls: ['./genre-add.component.css']
})
export class GenreAddComponent implements OnInit, OnDestroy {

  form: FormGroup;

  newGenreSub: Subscription;
  success: boolean;

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      genreName: new FormControl(null, {
        validators: [ Validators.required ]
      })
    });

    this.newGenreSub = this.booksService.getNewGenreUpdateListener()
      .subscribe(response => {
        this.success = response.message == 'success';
        //console.log(this.success);
      });

  }

  addGenre() {
    if(this.form.invalid)
      return;

    const genreData = {
      name: this.form.value.genreName
    }
    this.booksService.addNewGenre(genreData);

  }

  ngOnDestroy() {
    this.newGenreSub.unsubscribe();
  }

}
