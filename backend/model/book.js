const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const bookSchema = mongoose.Schema({
    title: { type: String, required: true },
    authors: { type: [String], required: true },
    imagePath: { type: String, default: 'http://localhost:3000/images/books/generic-book-cover.jpg' },
    publishDate: { type: Date, required: true },
    genres: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Genre', required: true }],
    description: { type: String, required: true },
    accepted: { type: Boolean, required: true }
});

bookSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Book', bookSchema);