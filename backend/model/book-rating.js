const mongoose = require('mongoose');

const bookRatingSchema = mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    bookId: { type: mongoose.Schema.Types.ObjectId, ref: 'Book', required: true },
    rating: { type: Number, required: true },
    comment: { type: String, required: false, default: '' }
});

module.exports = mongoose.model('BookRating', bookRatingSchema);