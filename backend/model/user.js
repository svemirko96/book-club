const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const userBookSchema = {
    book: { type: mongoose.Schema.Types.ObjectId, ref: 'Book', required: true },
    status: { type: String, default: 'toRead' },
    pagesRead: { type: Number, default: 0 },
    pagesCount: { type: Number, default: 0 }
};

const userSchema = mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    imagePath: { type: String, default: 'http://localhost:3000/images/users/generic-profile-image.png' },
    birthdate: { type: Date, required: true },
    country: { type: String, required: true },
    city: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    books: [userBookSchema],
    lastActive: { type: Date, default: Date.now },
    type: { type: String, required: true },
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);