const mongoose = require('mongoose');

const bookEventSchema = mongoose.Schema({
    name: { type: String, required: true },
    startDate: { type: Date, required: true },
    endDate: { type: Date, default: null },
    organizerId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    description: { type: String , default: '' },
    public: { type: Boolean, required: true }
});

module.exports = mongoose.model('BookEvent', bookEventSchema);