const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const request = require('request');

const User = require('../model/user');

const router = express.Router();

const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'
}

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = new Error('Invalid mime type');
        if(isValid) {
            error = null;
        }
        cb(error, 'images/profile-images');
    },
    filename: (req, file, cb) => {
        const name = file.originalname.toLowerCase().split(' ').join('-');
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const upload = multer({ storage: storage }).single('image');

router.post('/check-captcha', (req, res) => {

    if(
        req.body.captcha === undefined ||
        req.body.captcha === '' ||
        req.body.captcha === null
    ) {
        return res.json({ success: false, msg: 'No captcha received' });
    }

    const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

    request(verifyUrl, (err, response, body) => {
        body = JSON.parse(body);

        if(body.success !== undefined && !body.success) {
            return res.json({ success: false, msg: 'Captcha verification failed' });
        }

        return res.json({ success: true, msg: 'Captcha verification passed' });
    });

});

router.post('/register', upload, (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const url = req.protocol + '://' + req.get('host');
            let user;
            if(req.hasOwnProperty('file')) {
                user = new User({
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    username: req.body.username,
                    password: hash,
                    imagePath: url + '/images/users/' + req.file.filename,
                    birthdate: req.body.birthdate,
                    country: req.body.country,
                    city: req.body.city,
                    email: req.body.email,
                    books: [],
                    type: 'unverified'
                });
            } else {
                user = new User({
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    username: req.body.username,
                    password: hash,
                    birthdate: req.body.birthdate,
                    country: req.body.country,
                    city: req.body.city,
                    email: req.body.email,
                    books: [],
                    type: 'unverified'
                });
            }
            user.save()
                .then(document => {
                    console.log(`Uspesno dodat novi korisnik ${document.firstname} ${document.lastname}`);
                    return res.status(201).json({
                        userId: document._id
                    });
                })
                .catch(err => {
                    console.log(err);
                    console.log(`Nespesno dodat novi korisnik`);
                    return res.status(500).json({
                        error: err
                    });
                });
        });
});

router.post('/login', (req, res) => {
    let fetchedUser;
    User.findOne({ username: req.body.username }).then(user => {
        if(!user) {
            return res.status(401).json({
                message: 'userNotFound'
            });
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.password);
    }).then(result => {
        if(!result) {
            return res.status(401).json({
                message: 'invalidPassword'
            });
        }

        const token = jwt.sign(
            { userId: fetchedUser._id, username: fetchedUser.username },
            process.env.JWT_SECRET,
            { expiresIn: '1h' }
        );

        User.updateOne(
            { _id: fetchedUser._id },
            { lastActive: new Date() }
        ).then(updateRes => {
            return res.status(200).json({
                message: 'success',
                token: token,
                expiresIn: 3600,
                userId: fetchedUser._id
            });
        }).catch(err => {
            console.log('Failed updating last login: ' + err);

        })

    }).catch(err => {
        return res.status(401).json({
            message: 'Auth failed'
        });
    });
});

router.post('/edit-user', upload, (req, res) => {
    
    const url = req.protocol + '://' + req.get('host');
    let user, userUpdate;
    if(req.hasOwnProperty('file')) {
        userUpdate = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            imagePath: url + '/images/users/' + req.file.filename,
            birthdate: req.body.birthdate,
            country: req.body.country,
            city: req.body.city,
            email: req.body.email
        };
    } else {
        userUpdate = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            birthdate: req.body.birthdate,
            country: req.body.country,
            city: req.body.city,
            email: req.body.email
        };
    }
    User.updateOne({ _id: req.body.userId }, userUpdate)
        .then(result => {
            //console.log(result);
            return res.status(201).json({
                msg: 'success'
            });
        })
        .catch(err => {
            console.log(err);
            console.log('Nespesno izmenjeni podaci korisnika');
            return res.status(500).json({
                error: err
            });
        });
});

router.post('/new-password', (req, res) => {
    let fetchedUser;
    User.findOne({ _id: req.body.userId })
    .then(user => {
        if(!user) {
            return res.status(401).json({
                message: 'userNotFound'
            });
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.oldPassword, user.password);
    }).then(result => {
        if(!result) {
            return res.status(401).json({
                message: 'invalidPassword'
            });
        }

        bcrypt.hash(req.body.newPassword, 10).then(hash => {
            User.updateOne(
                { _id: req.body.userId },
                { password: hash }
            ).then(result => {
                return res.status(200).json({
                    message: 'success'
                });
            }).catch(err => {
                console.log(err);
                return res.status(500).json({
                    message: 'updateFailed'
                });
            });
        });
    }).catch(err => {
        console.log(err);
        return res.status(500).json({
            message: 'serverError'
        });
    });
});

router.get('/single/:id', (req, res) => {
    User.find({ _id: req.params.id }).then(users => {
        return res.status(200).json(users[0]);
    });
});

router.get('/search', (req, res) => {
    User.find().then(allUsers => {
        // console.log(allUsers);
        let queriedUsers = [];
        allUsers.forEach(user => {
            let skip = false;
            if(req.query.hasOwnProperty('firstname')) {
                if(user.firstname == req.query.firstname) {
                    queriedUsers.push(user);
                    skip = true;
                }
            }
            if(!skip && req.query.hasOwnProperty('lastname')) {
                if(user.lastname == req.query.lastname) {
                    queriedUsers.push(user);
                    skip = true;
                }
            }
            if(!skip && req.query.hasOwnProperty('username')) {
                if(user.username == req.query.username) {
                    queriedUsers.push(user);
                    skip = true;
                }
            }
            if(!skip && req.query.hasOwnProperty('email')) {
                if(user.email == req.query.email) {
                    queriedUsers.push(user);
                }
            }
        });
        return res.status(200).json(queriedUsers);
    });
});

router.post('/update-books', (req, res) => {
    let status = '';
    switch(req.body.action) {
        case 'addToList':
        case 'addToCurrentlyRead':
        case 'addToFinished':
            status =    (req.body.action == 'addToList') ? 'toRead' :
                        (req.body.action == 'addToCurrentlyRead') ? 'active' :
                        'finished';
            
            User.updateOne(
                { _id: req.body.userId },
                { $push: { books: {
                    book: req.body.bookId,
                    status: status,
                    pagesRead: 0,
                    pagesCount: 100
                }}}).then(result => {
                    return res.status(200).json({
                        message: 'Successfully added book to the list'
                    });
                }).catch(err => {
                    return res.status(500).json({
                        message: 'Failed to add book to the list'
                    });
                });
            break;
        case 'removeFromList':
            User.updateOne(
                { _id: req.body.userId },
                { $pull: { books: {
                    book: req.body.bookId
                }}}).then(result => {
                    return res.status(200).json({
                        message: 'Successfully removed book to the list'
                    });
                }).catch(err => {
                    return res.status(500).json({
                        message: 'Failed to remove book to the list'
                    });
                });
            break;
        case 'startReading':
        case 'finishReading':
            status = req.body.action == 'startReading' ? 'active' : 'finished';
            User.updateOne(
                { _id: req.body.userId, 'books.book': req.body.bookId },
                { $set: {
                    'books.$.status': status
                }}).then(result => {
                    return res.status(200).json({
                        message: 'Successfully updated book in the list'
                    });
                }).catch(err => {
                    return res.status(500).json({
                        message: 'Failed to update book in the list'
                    });
                });
            break;
        case 'updatePages':
            User.updateOne(
                { _id: req.body.userId, 'books.book': req.body.bookId },
                { $set: {
                    'books.$.pagesRead': req.body.pagesRead,
                    'books.$.pagesCount': req.body.pagesCount,
                }}).then(result => {
                    return res.status(200).json({
                        message: 'Successfully updated book in the list'
                    });
                }).catch(err => {
                    return res.status(500).json({
                        message: 'Failed to update book in the list'
                    });
                });
            break;
        default:
            return res.status(500).json({
                message: 'Invalid action requested'
            });
    }

});

router.get('/all-users', (req, res) => {
    User.find().then(users => {
        return res.status(200).json({ users: users });
    }).catch(err => {
        return res.status(500).json({ message: 'Failed to fetch users' });
    });
});

router.post('/user-management', (req, res) => {
    //console.log(req.body);
    let userId = req.body.userId;
    let action = req.body.action;
    switch(action) {
        case 'accept':
        case 'promote':
        case 'demote':
            let newType = (action == 'accept' || action == 'demote') ? 'user' : 'mod';
            User.updateOne(
                { _id: userId },
                { type: newType }
            ).then(response => {
                return res.status(200).json({
                    message: 'success'
                });
            }).catch(err => {
                return res.status(500).json({
                    message: 'serverError'
                });
            });
            break;
        case 'reject':
            User.deleteOne({ _id: userId }).then(response => {
                return res.status(200).json({
                    message: 'success'
                });
            }).catch(err => {
                return res.status(500).json({
                    message: 'serverError'
                });
            });
            break;
        default:
            return res.status(400).json({
                message: 'badRequest'
            });
    }
});

module.exports = router;