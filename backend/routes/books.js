const express = require('express');
const multer = require('multer');

const Book = require('../model/book');
const Genre = require('../model/genre');
const BookRating = require('../model/book-rating');
const BookEvent = require('../model/book-event');

const router = express.Router();

const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'
}

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = new Error('Invalid mime type');
        if(isValid) {
            error = null;
        }
        cb(error, 'images/cover-images');
    },
    filename: (req, file, cb) => {
        const name = file.originalname.toLowerCase().split(' ').join('-');
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name + '-' + Date.now() + '.' + ext);
    }
});

const upload = multer({ storage: storage }).single('image');

router.get('/genres', (req, res) => {
    Genre.find().then(data => {
        return res.status(200).json({
            genres: data
        });
    });
});

router.get('/book/:id', (req, res) => {
    Book.findById(req.params.id).then(book => {
        return res.status(200).json(book);
    }).catch(err => {
        console.log(err);
        return res.status(404).json({
            message: 'notFound'
        });
    });
});

router.get('/search', (req, res) => {
    Book.find().then(allBooks => {
        // console.log(allBooks);
        let queriedBooks = [];
        allBooks.forEach(book => {
            let skip = false;
            if(req.query.hasOwnProperty('title')) {
                if(book.title.indexOf(req.query.title) != -1) {
                    queriedBooks.push(book);
                    skip = true;
                }
            }
            if(!skip && req.query.hasOwnProperty('author')) {
                if(book.authors.includes(req.query.author)) {
                    queriedBooks.push(book);
                    skip = true;
                }
            }
            if(!skip && req.query.hasOwnProperty('genre')) {
                if(book.genres.includes(req.query.genre)) {
                    queriedBooks.push(book);
                }
            }
        });
        return res.status(200).json({ books: queriedBooks });
    });
});

router.get('/all-books', (req, res) => {
    Book.find().then(books => {
        res.status(200).json(books);
    });
});

router.post('/new-book', upload, (req, res) => {
    // console.log(req.body);
    const url = req.protocol + '://' + req.get('host');
    let book = new Book({
        title: req.body.title,
        authors: req.body.authors.split(','),
        publishDate: req.body.publishDate,
        genres: req.body.genres.split(','),
        description: req.body.description,
        accepted: false
    });
    if(req.hasOwnProperty('file')) {
        book.imagePath = url + '/images/books/' + req.file.filename;
    }
    // console.log(book);
    book.save()
        .then(document => {
            console.log(`Uspesno dodata nova knjiga ${document.title}`);
            return res.status(201).json({
                bookId: document._id,
                message: 'success'
            });
        })
        .catch(err => {
            console.log(err);
            console.log(`Nespesno dodata nova knjiga`);
            return res.status(500).json({
                message: 'failure'
            });
        });
});

router.post('/edit-book', upload, (req, res) => {

    const url = req.protocol + '://' + req.get('host');
    let bookUpdate = {
        title: req.body.title,
        authors: req.body.authors.split(','),
        publishDate: req.body.publishDate,
        genres: req.body.genres.split(','),
        description: req.body.description,
        accepted: req.body.accepted
    };
    if(req.hasOwnProperty('file')) {
        bookUpdate.imagePath = url + '/images/books/' + req.file.filename;
    }
    // console.log(book);
    Book.updateOne({ _id: req.body.bookId }, bookUpdate)
        .then(result => {
            //console.log(result);
            return res.status(201).json({
                msg: 'success'
            });
        })
        .catch(err => {
            console.log(err);
            console.log('Nespesno izmenjeni podaci korisnika');
            return res.status(500).json({
                error: err
            });
        });

});

router.post('/verify-book', (req, res) => {
    Book.updateOne(
        { _id: req.body.bookId },
        { accepted: true }
    ).then(result => {
        return res.status(200).json({
            message: 'success'
        });
    }).catch(err => {
        console.log('Failed to verify book: ' + err);
        return res.status(500).json({
            message: 'failure'
        });
    });
});

router.post('/book-rating', (req, res) => {
    BookRating.updateOne(
        { userId: req.body.userId, bookId: req.body.bookId },
        {
            userId: req.body.userId,
            bookId: req.body.bookId,
            rating: req.body.rating,
            comment: req.body.comment
        },
        { upsert: true, setDefaultsOnInsert: true }
    ).then(result => {
        return res.status(200).json({
            message: 'Successfully saved book rating'
        });
    }).catch(err => {
        return res.status(500).json({
            message: 'Failed to save book rating'
        });
    });
});

router.get('/book-rating/:id', (req, res) => {
    BookRating.find({ bookId: req.params.id }).then(ratings => {
        return res.status(200).json(ratings);
    });
});

router.get('/book-ratings-by-user/:id', (req, res) => {
    BookRating.find({ userId: req.params.id }).then(ratings => {
        return res.status(200).json(ratings);
    });
});

router.post('/new-genre', (req, res) => {
    Genre.find({ name: req.body.name }).then(genres => {
        if(genres.length > 0) {
            return res.status(200).json({
                message: 'alreadyExists'
            });
        }

        let genre = new Genre({
            name: req.body.name
        });
        genre.save().then(result => {
            return res.status(200).json({
                message: 'success'
            });
        }).catch(err => {
            console.log('Failed to add new genre: ' + err);
            return res.status(500).json({
                message: 'serverError'
            });
        });

    }).catch(err => {
        console.log('Failed to fetch genres: ' + err);
        return res.status(500).json({
            message: 'serverError'
        });
    });
});

router.post('/delete-genre', (req, res) => {
    Genre.deleteOne({ _id: req.body.genreId }).then(result => {
        return res.status(200).json({
            message: 'success'
        });
    }).catch(err => {
        console.log('Failed to delete genre: ' + err);
        return res.status(500).json({
            message: 'failure'
        });
    });
});

router.get('/events', (req, res) => {
    BookEvent.find().then(events => {
        return res.status(200).json(events);
    }).catch(err => {
        console.log('Failed to fetch book events: ' + err);
        return res.status(500).json({
            message: 'failure'
        });
    });
});

module.exports = router;