const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();

const userRoutes = require('./routes/user');
const booksRoutes = require('./routes/books');

const server = express();

mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to database!');
    })
    .catch(() => {
        console.log('Connection to database failed');
    });

server.use(express.urlencoded({ extended: true }));
server.use(express.json());
server.use('/images/users', express.static(path.join(__dirname, '/images/profile-images')));
server.use('/images/books', express.static(path.join(__dirname, '/images/cover-images')));

server.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
    next();
});

// const Genre = require('./model/genre');
// let genre = new Genre({
//     name: 'Zdravlje, porodica i životni stil'
// });
// genre.save();

// const Book = require('./model/book');
// let book = new Book({
//     title: 'Kompas ishrane',
//     authors: ['Bas Kast'],
//     imagePath: 'http://localhost:3000/images/books/kompas-ishrane.jpg',
//     publishDate: '2020-05-22',
//     genre1: ['5f5282c68f437835e42acfc5'],
//     description: 'Kada je Bas Kast, novinar posvećen naučnim temama, osetio probadanje u grudima, ozbiljno se zabrinuo i ubrzo mu se nametnulo ključno pitanje: da li je lošom ishranom upropastio svoje zdravlje?'
// });
// book.save();

// const BookEvent = require('./model/book-event');
// let bookEvent = new BookEvent({
//     name: 'Književno veče 3',
//     startDate: '2020-09-28',
//     endDate: '2020-10-5',
//     organizerId: '5f57892835916825641708ba',
//     description: 'Književno veče #3',
//     public: true
// });
// bookEvent.save();

server.use('/api/books', booksRoutes);
server.use('/api/user', userRoutes);

server.listen(3000);