A simple social network type of app where people can search books, keep track of the books that they are currently reading, the books they have read and the books they plan to read in the future.
Books can be rated and commented on by users that have read at least half of the book being rated.

There is also foundation for further implementing other features such as public and private events that users can attend at certain conditions as well as subscrubing to other users to get notifications of their activity, etc.
